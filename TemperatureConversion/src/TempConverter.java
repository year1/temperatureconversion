
public class TempConverter 
{
	/**
	 * Converts CelSius to FahRenheit
	 * @param C = Temperature for CelSius. Only within -273 >= C <= 100 
	 * @return	Temperature in FahRenheit. -1000 for out of bounds input values.
	 */
	public static double convertCelsius(double C)
	{
		if (C < -273 || C > 100)
			return -1000;
		else
			return C*9/5+32;
	}

	/**
	 * Converts FahRenheit to CelSius
	 * @param F = Temperature for FahRenheit. Only within -459.4 >= C <= 212 
	 * @return	Temperature in CelSius. -1000 for out of bounds input values.
	 */
	public static double convertFahrenheit(double F)
	{
		if (F < -459.4 || F > 212)
			return -1000;
		else
			return (F-32)*5/9;
	}

}
