import junit.framework.TestCase;

public class TempConverterTest extends TestCase 
{
	// Test for CelSius Converter
	/**
	 * Test 001
	 * Test for Maximum lower rejected boundary value
	 * Input CelSius = -Double.MAX_VALUE
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter001() 
	{
		assertEquals(-1000,TempConverter.convertCelsius(-Double.MAX_VALUE),0.005);
	}
	/**
	 * Test 002
	 * Test for Minimum lower rejected boundary value
	 * Input CelSius = -273.01
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter002() 
	{
		assertEquals(-1000,TempConverter.convertCelsius(-273.01),0.005);
	}
	/**
	 * Test 003
	 * Test for Minimum accepted boundary value
	 * Input CelSius = -273
	 * Output expected = -459.4
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter003() 
	{
		assertEquals(-459.4,TempConverter.convertCelsius(-273),0.005);
	}
	/**
	 * Test 004
	 * Test for Maximum accepted boundary value
	 * Input CelSius = 100
	 * Output expected = 212
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter004() 
	{
		assertEquals(212,TempConverter.convertCelsius(100),0.005);
	}
	/**
	 * Test 005
	 * Test for Minimum higher rejected boundary value
	 * Input CelSius = 100.01
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter005() 
	{
		assertEquals(-1000,TempConverter.convertCelsius(100.01),0.005);
	}
	/**
	 * Test 006
	 * Test for Maximum accepted value
	 * Input CelSius = Double.MAX_VALUE
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter006() 
	{
		assertEquals(-1000,TempConverter.convertCelsius(Double.MAX_VALUE),0.005);
	}

	// Test for FahRenheit Converter
	/**
	 * Test 007
	 * Test for Maximum lower rejected boundary value
	 * Input FahRenheit = -Double.MAX_VALUE
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter007() 
	{
		assertEquals(-1000,TempConverter.convertFahrenheit(-Double.MAX_VALUE),0.005);
	}
	/**
	 * Test 008
	 * Test for Minimum lower rejected boundary value
	 * Input FahRenheit = -459.41
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter008() 
	{
		assertEquals(-1000,TempConverter.convertFahrenheit(-459.41),0.005);
	}
	/**
	 * Test 009
	 * Test for Minimum accepted boundary value
	 * Input FahRenheit = -459.4
	 * Output expected = -273
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter009() 
	{
		assertEquals(-273,TempConverter.convertFahrenheit(-459.4),0.005);
	}
	/**
	 * Test 010
	 * Test for Maximum accepted boundary value
	 * Input FahRenheit = 212
	 * Output expected = 100
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter010() 
	{
		assertEquals(100,TempConverter.convertFahrenheit(212),0.005);
	}
	/**
	 * Test 011
	 * Test for Minimum higher rejected boundary value
	 * Input FahRenheit = 212.01
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter011() 
	{
		assertEquals(-1000,TempConverter.convertFahrenheit(212.01),0.005);
	}
	/**
	 * Test 012
	 * Test for Maximum accepted value
	 * Input FahRenheit = Double.MAX_VALUE
	 * Output expected = -1000
	 * Delta = 0.005 ( ensure to test for 2 decimal place error )
	 */
	public void testTempConverter012() 
	{
		assertEquals(-1000,TempConverter.convertFahrenheit(Double.MAX_VALUE),0.005);
	}
}
